# -*- coding: utf-8 -*-

from django import forms
from django.utils import timezone

from surveys.models import Answer


class AnswerForm(forms.Form):
    """Formar para validar las respuestas de la encuesta"""

    full_name = forms.CharField(
        max_length=50, required=False, label="Nombre")
    email = forms.EmailField(
        max_length=40, required=False, label="Correo Electronico")
    age = forms.IntegerField(
        min_value=0, max_value=100, required=False, label="Edad")
    brothers = forms.IntegerField(
        min_value=0, max_value=50, required=False, label="¿Cuantos hermanos tienes?")
    website = forms.URLField(
        max_length=40, required=False, label="Página Web")
    social_network = forms.CharField(
        required=False, max_length=20, label="Red Social Favorita")
    hobbies = forms.CharField(
        widget=forms.Textarea, max_length=50, required=False, label="Pasatiempos favoritos")
    about_me = forms.CharField(
        widget=forms.Textarea, required=False, label="Describe tu personalidad")
    singer = forms.CharField(
        max_length=40, required=False, label="¿Cantante favorito?")
    actor = forms.CharField(
        max_length=40, required=False, label="¿Actor favorito?")
    survey_session = forms.RegexField(
        widget=forms.HiddenInput(),
        regex=r'^([a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12})$',
        error_messages={'invalid': u"Sesión inválida."}
    )

    def __init__(self, *args, **kwargs):
        """Inicializacion del formulario"""

        self.instance = kwargs.pop('instance', None)
        super(AnswerForm, self).__init__(*args, **kwargs)

        if self.instance:
            # Inicializar el formulario con datos existentes
            for field in self.fields:
                self.fields[field].initial = getattr(self.instance, field)

    def save(self, completed=False, commit=True):
        """Guardar la respuesta en un modelo de cassandra"""

        # Obtener o crear instancia del modelo
        answer = self.instance if self.instance else Answer()

        # Setear los atributos
        for key, value in self.data.iteritems():
            if key in self.cleaned_data:
                # change empty
                value = None if self.cleaned_data[key] == "" else self.cleaned_data[key]
                setattr(answer, key, value)
        answer.completed = completed

        if commit:
            answer.save()

        return answer


class AnswerCompletedForm(AnswerForm):
    """Form to validate the survey"""

    required_fields = ['full_name', 'email', 'social_network', 'age', 'brothers']

    def __init__(self, *args, **kwargs):
        """Inicializacion del formulario"""
        super(AnswerCompletedForm, self).__init__(*args, **kwargs)
        for field_name in self.required_fields:
            self.fields[field_name].required = True


