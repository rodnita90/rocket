# -*- coding: utf-8 -*-

from django.http import *
from django.shortcuts import render
from django.urls import reverse

from .forms import AnswerForm, AnswerCompletedForm
from .models import Answer
from .utils import save_session, process_answer, build_filters_to_answers


def survey(request):
    """Desplegar encuesta o guardar respuestas de la encuesta"""
    # Guardar Respuesta
    if request.method == 'POST' and not request.is_ajax():
        form, success = process_answer(AnswerCompletedForm, request.POST, completed=True)
        if success:
            return HttpResponseRedirect(reverse('thanks'))
        session = request.POST.get('survey_session', None)
    # Desplegar encuesta
    elif request.method == 'GET' and not request.is_ajax():
        session = save_session(request)     # Crear sesion
        form = AnswerForm()
    # NotAllowed
    else:
        return HttpResponseNotAllowed(['GET, POST'], "<h1>Not Allowed</h1>")

    return render(request, 'survey.html', {'form': form, 'survey_session': session})


def survey_log(request):
    """Guardar a través de AJAX las respuestas temporales"""
    if request.method == 'POST' and request.is_ajax():
        form, success = process_answer(AnswerForm, request.POST)
        if success:
            return JsonResponse({'success': True}, status=200)
        else:
            return JsonResponse({'success': False, 'errors': form.errors}, status=400)
    else:
        return JsonResponse({'success': False}, status=405)


def survey_list(request):
    """Desplegar informacion de formularios"""
    if request.method == 'GET':
        filters = build_filters_to_answers(request.GET)

        # Incorrect param
        if filters is False:
            return HttpResponseNotFound("<h1>Not Found</h1>")

        context = {'surveys': Answer.objects.filter(**filters)}
        return render(request, 'survey_list.html', context)
    else:
        return HttpResponseNotAllowed(['GET, POST'], "<h1>Method Not Allowed</h1>")