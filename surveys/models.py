# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import uuid

from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model as CassandraModel
from django.utils import timezone


class Session(CassandraModel):
    """Modelo para almacenar las sesiones que ha tenido la encuesta"""

    id = columns.UUID(primary_key=True, default=uuid.uuid4)
    created_at = columns.DateTime(default=timezone.now())
    user_agent = columns.Text()
    ip = columns.Inet()


class Answer(CassandraModel):
    """Modelo para almacenar las respuestas de la encuesta"""

    id = columns.UUID(primary_key=True, default=uuid.uuid4)
    survey_session = columns.UUID(required=True, index=True)
    created_at = columns.DateTime(default=timezone.now())
    completed = columns.Boolean(default=False, index=True)
    about_me = columns.Text()
    full_name = columns.Text(max_length=50)
    hobbies = columns.Text(max_length=50)
    singer = columns.Text(max_length=40)
    actor = columns.Text(max_length=40)
    age = columns.TinyInt()
    brothers = columns.TinyInt()
    social_network = columns.Text(max_length=20)
    website = columns.Text(max_length=40)
    email = columns.Text(max_length=40)
