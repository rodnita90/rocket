# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.views.generic import TemplateView

from . import views


urlpatterns = [
    url(r'^$', views.survey, name='survey'),
    url(r'^survey$', views.survey_list, name='survey_list'),
    url(r'^survey/log$', views.survey_log, name='survey_log'),
    url(r'^survey/thanks$', TemplateView.as_view(template_name="thanks.html"), name="thanks")
]
