# -*- coding: utf-8 -*-
from ipware.ip import get_real_ip

from surveys.models import Session, Answer


def save_session(request):
    """
    Guardar la visita a la encuesta
    @param request datos de la peticion
    @return uuid Id de la Sesion
    """

    session = Session(
        user_agent=request.META['HTTP_USER_AGENT'],
        ip=get_real_ip(request)
    )
    session.save()
    return session.id


def process_answer(answer_form, data, completed=False):
    """
    Procesar la respuesta enviada en el POST
    @param answer_form Form Forma para validar los datos
    @param data dict Datos a validar
    @param completed Bool Indica si ya termino la encuesta
    @return Form, Bool Instancia de la forma y Bool indica si hubo errores
    """

    # Obtener la sesión de la encuesta
    session = data.get('survey_session', None)

    # Crear el formulario de la respuesta
    try:
        instance = Answer.objects.get(survey_session=session)
    except Answer.DoesNotExist:
        form = answer_form(data)
    else:
        form = answer_form(data, instance=instance)

    # Guardar
    if form.is_valid():
        form.save(completed=completed)
        return form, True

    return form, False


def build_filters_to_answers(params):
    """
    Contruir query para lista de respuestas
    @param params Dict request.GET
    @return dict query para filtrar
    """
    status = params.get('status', 'all')
    if status == 'complete':
        return {"completed": True}
    elif status == 'incomplete':
        return {"completed": False}
    elif status == 'all':
        return {}
    else:
        return False
