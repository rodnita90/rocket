$(document).ready(function() {

    $('#quiz input, #quiz textarea').on('blur',function(){
        var data = {
            csrfmiddlewaretoken: getCookie('csrftoken'),
            survey_session: $('input[name="survey_session"]').val()
        };
        data[$(this).prop('name')] = $(this).val();
        var element = $(this);
        $.post($('#quiz').attr('log-url'), data).done(
            function(response){
                console.log(':)');
             }
           ).fail(function(response){
               element.css("border", "1px solid red");
        });
    });

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
});